# Redis Cluster Graph

Generates a graph representing a Redis cluster topology.

```
# ./redis-cluster-graph
┌────────────────────────────────────────────┐     ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓     ┌────────────────────────────────────────────┐
│                 bb11:6380                  │     ┃          bb13:6379 [10923-16383]           ┃     │                 bb12:6381                  │
│ (365277fd5d5de63209011d35aed2f70972416bfd) │ ──> ┃ (b10797c32cd9fca71263b66de1292923e7500485) ┃ <── │ (8bc9e1c05a71b141b64bfc6a4eba4c6a434031f2) │
└────────────────────────────────────────────┘     ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛     └────────────────────────────────────────────┘
┌────────────────────────────────────────────┐     ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓     ┌────────────────────────────────────────────┐
│                 bb12:6380                  │     ┃             bb11:6379 [0-5460]             ┃     │                 bb13:6381                  │
│ (69f251d94ef235544c779f30a0f2fdfaf6a6d677) │ ──> ┃ (208913f46c28d31da663136de25dc297c263459e) ┃ <── │ (aa362ca5e065b6e2ab41c1f9f3e8b256231f0078) │
└────────────────────────────────────────────┘     ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛     └────────────────────────────────────────────┘
┌────────────────────────────────────────────┐     ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓     ┌────────────────────────────────────────────┐
│                 bb13:6380                  │     ┃           bb12:6379 [5461-10922]           ┃     │                 bb11:6381                  │
│ (724d3aff068b92d7dd47328bd9f2f4531119fd81) │ ──> ┃ (f0cd053027ac7275489f171ea92e1e68c90690bd) ┃ <── │ (e2ca622f947a64a4a56c3d70f0a79e2217f24670) │
└────────────────────────────────────────────┘     ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛     └────────────────────────────────────────────┘
```

## Usage

```

# Connect to local instance listening at default port
$ redis-cluster-graph

# Connect to an instance in other location
$ redis-cluster-graph 127.17.0.4:6380
```

## Dependencies

The command is written in Python. It works both with Python 2 and 3.

The graph is generated with the great [Graph-Easy](https://github.com/ironcamel/Graph-Easy) tool.

In Debian-based systems, just `apt-get install libgraph-easy-perl`.


## Development

Development happens on [Gitlab](https://gitlab.com/sbitio/redis-cluster-graph).

Please log issues for any bug report, feature or support request.

Merge requests are welcome.


## License

MIT License, see LICENSE file

## Contact

Use contact form on http://sbit.io
